defmodule Nopek.MixProject do
  use Mix.Project

  def project do
    [
      app: :nopek,
      version: "0.1.0",
      elixir: "~> 1.15",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: aliases()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Nopek.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:nimble_publisher, "~> 1.1"},
      {:phoenix_live_view, "~> 0.20.1"},
      {:esbuild, "~> 0.8"},
      {:tailwind, "~> 0.2"},
      {:bandit, "~> 1.1"},
      {:exsync, "~> 0.2"}
    ]
  end

  defp aliases() do
    [
      "site.build": ["build", "tailwind default --minify", "esbuild default --minify"]
    ]
  end
end
