var btn = document.getElementById('prog-btn');

if(btn) {
  btn.addEventListener('click', _ => {
    document.getElementById('prog').scrollIntoView({ behavior: 'smooth'});
  });
}