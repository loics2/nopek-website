import Config

config :esbuild,
  version: "0.17.11",
  default: [
    args:
      ~w(app.js --bundle --target=es2017 --outdir=../public/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

config :tailwind,
  version: "3.2.4",
  default: [
    args: ~w(
      --config=tailwind.config.js
      --input=app.css
      --output=../public/assets/app.css
    ),
    cd: Path.expand("../assets", __DIR__)
  ]

config :exsync,
  src_monitor: true,
  extra_extensions: [".md", ".js", ".css"],
  addition_dirs: ["/pages", "/assets"],
  reload_callback: {Nopek.DevServer, :handle_reload, []}
