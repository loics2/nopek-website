defmodule Mix.Tasks.Serve do
  use Mix.Task

  @shortdoc "Starts a dev server"

  @impl true
  def run(args) do
    Tailwind.install_and_run(:default, [])
    Esbuild.install_and_run(:default, [])
    Mix.Tasks.Build.run([])
    Mix.Tasks.Run.run(args ++ run_args())
  end

  defp iex_running? do
    Code.ensure_loaded?(IEx) and IEx.started?()
  end

  defp run_args do
    if iex_running?(), do: [], else: ["--no-halt"]
  end
end
