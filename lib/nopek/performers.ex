defmodule Nopek.Performers do
  alias Nopek.Performers.Performer

  use NimblePublisher,
    build: Performer,
    from: "./pages/performers/*.md",
    as: :performers

  def all_performers(), do: @performers
end
