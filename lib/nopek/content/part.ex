defmodule Nopek.Content.Part do
  @enforce_keys [:id, :title, :body]
  defstruct [:id, :title, :body]

  def build(filename, attrs, body) do
    id = Path.basename(filename, ".md")

    struct!(__MODULE__, [id: id, body: body] ++ Map.to_list(attrs))
  end
end
