defmodule Nopek.Application do
  require Logger

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      {Bandit, plug: Nopek.DevServer, scheme: :http, port: 3000}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Nopek.Supervisor]
    Logger.info("serving dev site at http://localhost:3000")
    Supervisor.start_link(children, opts)
  end
end
