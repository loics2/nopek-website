defmodule Nopek.Performers.Performer do
  @enforce_keys [:id, :name, :body, :description, :path, :image]
  defstruct [:id, :name, :body, :description, :path, :image]

  def build(filename, attrs, body) do
    path = Path.rootname(filename)
    id = Path.basename(filename, ".md")
    path = path <> ".html"

    struct!(__MODULE__, [id: id, body: body, path: path] ++ Map.to_list(attrs))
  end
end
