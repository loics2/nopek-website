defmodule Nopek.DevServer do
  @moduledoc false

  use Plug.Router

  def __mix_recompile__?, do: true

  plug(Plug.Logger, log: :info)
  plug(Plug.Static, at: "/", from: "public")
  plug(:match)
  plug(:dispatch)

  get "/*path" do
    path = Path.join([File.cwd!(), "public"] ++ conn.path_info ++ ["index.html"])

    if File.exists?(path) do
      send_file(conn, 200, path)
    else
      path = Path.join([File.cwd!(), "public", "404.html"])
      send_file(conn, 404, path)
    end
  end

  def handle_reload() do
    Nopek.build()
    Tailwind.install_and_run(:default, [])
    Esbuild.install_and_run(:default, [])
  end
end
