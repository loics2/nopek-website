defmodule Nopek.Components do
  use Phoenix.Component

  alias Phoenix.HTML.Form

  attr :title, :string, default: ""
  slot :image
  slot :subtitle
  slot :actions
  def hero(assigns) do
    ~H"""
    <section class="bg-background dark:bg-text">
      <div class="grid max-w-screen-xl px-4 py-8 mx-auto lg:gap-8 xl:gap-0 lg:py-16 lg:grid-cols-12">
          <div class="mr-auto place-self-center lg:col-span-7">
            <h1 class="max-w-2xl mb-4 text-4xl font-extrabold tracking-tight leading-none md:text-5xl xl:text-6xl text-text dark:text-background"><%= @title %></h1>
            <p class="max-w-2xl mb-6 font-light text-text-500 lg:mb-8 md:text-lg lg:text-xl dark:text-background-400"><%= render_slot(@subtitle)%></p>
            <%= render_slot(@actions) %>
          </div>
          <div class="hidden lg:mt-0 lg:col-span-5 lg:flex">
            <%= render_slot(@image) %>
          </div>
      </div>
    </section>
    """
  end

  attr :type, :string, values: ~w(submit button), default: "button"
  attr :color, :string, values: ~w(primary secondary), default: "primary"
  attr :class, :string, default: nil
  attr :rest, :global

  slot :inner_block
  def button(assigns) do
    ~H"""
    <button type={@type} class={["inline-flex items-center justify-center px-5 py-3 mr-3 text-base font-medium text-center rounded-lg focus:ring-4 transition duration-300 ease-in-out hover:-translate-y-3", @class] ++ color(@color)} {@rest}>
      <%= render_slot(@inner_block) %>
    </button>
    """
  end

  defp color("primary") do
    [
      "text-background bg-primary hover:shadow-lg hover:shadow-primary/50 focus:ring-primary-300 dark:focus:ring-primary-900"
    ]
  end

  attr :name, :string
  attr :desc, :string
  attr :image, :string
  attr :url, :string, default: nil

  slot :inner_block
  def card(%{url: nil} = assigns) do
    ~H"""
    <div class="max-w-sm group">
      <div class="relative rounded-lg overflow-hidden">
        <img src={@image} alt={"image de #{@name}"}/>
        <.circle class="transition-transform duration-200 group-hover:duration-200 fill-primary" />
        <.circle class="transition-transform duration-200 group-hover:duration-300 group-hover:delay-150 fill-secondary" offset={100} />

        <div class="absolute p-5 h-1/2 bottom-0 left-0 translate-y-full group-hover:translate-y-0 transition-transform duration-200 group-hover:duration-300 group-hover:delay-150">
          <h5 class="mb-2 text-2xl font-bold tracking-tight text-text"><%= @name %></h5>
          <p :if={@inner_block == []} class="mb-3 font-normal text-text-700"><%= @desc %></p>
          <div :if={@inner_block != []} class="mb-3 font-normal text-text-700">
            <%= render_slot(@inner_block) %>
          </div>
        </div>
      </div>
    </div>
    """
  end

  def card(assigns) do
    ~H"""
    <div class="max-w-sm group">
      <div class="relative rounded-lg overflow-hidden aspect-a4">
        <a href={@url} class="h-full w-full">
          <img src={@image} alt={"image de #{@name}"} class="h-full w-full object-cover"/>
        </a>
        <.circle class="transition-transform duration-200 group-hover:duration-200 fill-primary" />
        <.circle class="transition-transform duration-200 group-hover:duration-300 group-hover:delay-150 fill-secondary" offset={100} />

        <div class="absolute p-5 h-1/3 bottom-0 left-0 translate-y-full group-hover:translate-y-0 transition-transform duration-200 group-hover:duration-300 group-hover:delay-150">
          <a href={@url}>
            <h5 class="mb-2 text-2xl font-bold tracking-tight text-text"><%= @name %></h5>
          </a>
          <p class="mb-3 font-normal text-text-700"><%= @desc %></p>
          <a href={@url} class="inline-flex items-center py-2 font-medium text-center text-primary underline underline-offset-4 group-hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
            Plus d'info
          </a>
        </div>
      </div>
    </div>
    """
  end

  attr :offset, :integer, default: 0
  attr :class, :string, default: nil
  defp circle(assigns) do
    ~H"""
    <svg viewBox={"0 0 500 500"} class={["w-full absolute bottom-0 left-0 translate-y-full group-hover:translate-y-0 overflow-visible drop-shadow-lg", @class]} xmlns="http://www.w3.org/2000/svg">
      <defs>
        <filter id="shadow">
          <feOffset result="offOut" in="SourceAlpha" dx="0" dy="5" />
          <feGaussianBlur result="blurOut" in="offOut" stdDeviation="10" />
          <feBlend in="SourceGraphic" in2="blurOut" mode="normal" />
        </filter>
      </defs>
      <circle cx="250" cy={510 + @offset} r="500" filter="url(#shadow)"/>
    </svg>
    """
  end

  attr :class, :string, default: nil

  slot :title
  slot :subtitle
  slot :inner_block

  def empty(assigns) do
    ~H"""
    <div class={["h-full w-full pt-28 flex items-center justify-center", @class]}>
      <div class="flex flex-col items-center">
        <h6 class="mt-4">
          <%= render_slot(@title) %>
        </h6>
        <p class="mt text-sm leading-6 text-text dark:text-background">
          <%= render_slot(@subtitle) %>
        </p>
        <div class="mt-6">
          <%= render_slot(@inner_block) %>
        </div>
      </div>
    </div>
    """
  end

  slot(:logo)

  slot :col, required: true do
    attr :label, :string
  end

  def footer(assigns) do
    ~H"""
    <footer class="p-4 bg-text-900 sm:p-6 md:mt-0">
      <div class="flex flex-col md:flex-row md:justify-between container mx-auto">
        <div class="flex items-center justify-center md:w-1/4">
          <%= render_slot(@logo) %>
        </div>
        <div class="flex flex-col justify-center md:grid md:grid-cols-3 md:w-1/2">
          <div :for={col <- @col}>
            <h2 class="text-sm font-semibold uppercase text-background text-center">
              <%= col.label %>
            </h2>
            <div class="flex items-center justify-center h-full p-8">
              <%= render_slot(col) %>
            </div>
          </div>
        </div>
      </div>
      <hr class="my-6 sm:mx-auto border-background-700 lg:my-8 container mx-auto" />
      <span class="block text-sm text-center text-background-400">
        Viens au Nopek!
      </span>
    </footer>
    """
  end

  attr :name, :string
  attr :label, :string
  attr :id, :string, default: nil
  attr :type, :string, default: "text"
  attr :required, :boolean, default: false
  attr :class, :string, default: "w-full"
  attr :value, :any, default: nil
  attr :options, :list, doc: "the options to pass to Phoenix.HTML.Form.options_for_select/2"
  attr :prompt, :string, default: nil, doc: "the prompt for select inputs"
  attr :rest, :global,
    include: ~w(accept autocomplete capture cols disabled form list max maxlength min minlength
                multiple pattern placeholder readonly rows size step checked)

  def input(%{type: "select"} = assigns) do
    ~H"""
    <div class={["my-2", @class]}>
      <.label for={@id} required={@required}><%= @label %></.label>
      <select
        id={@id}
        name={@name}
        class={[
          "border-text-500 focus:border-primary-600 focus:ring-primary-800/5 bg-background-50",
          "dark:border-background-500 dark:focus:border-primary-600 dark:focus:ring-primary-600/5 dark:bg-text-900",
          "mt-2 block w-full rounded-lg py-[7px] px-[11px] border-2",
          "text-text-900 dark:text-background-100 focus:outline-none focus:ring-4 sm:text-sm sm:leading-6"
        ]}
        {@rest}
      >
        <option :if={@prompt} value=""><%= @prompt %></option>
        <%= Form.options_for_select(@options, @value) %>
      </select>
    </div>
    """
  end

  def input(assigns) do
    ~H"""
    <div class={["my-2", @class]}>
      <.label for={@id} required={@required}>
        <%= @label %>
      </.label>
      <input
        type={@type}
        name={@name}
        id={@id}
        value={Form.normalize_value(@type, @value)}
        class={[
          "border-text-500 focus:border-primary-600 focus:ring-primary-800/5 bg-background-50",
          "dark:border-background-500 dark:focus:border-primary-600 dark:focus:ring-primary-600/5 dark:bg-text-900",
          "mt-2 block w-full rounded-lg py-[7px] px-[11px] border-2",
          "text-text-900 dark:text-background-100 focus:outline-none focus:ring-4 sm:text-sm sm:leading-6"
        ]}
        {@rest}
      />
    </div>
    """
  end

  @doc """
  Renders a label.
  """
  attr :for, :string, default: nil
  attr :required, :boolean, default: false
  slot :inner_block, required: true

  def label(assigns) do
    ~H"""
    <label for={@for} class={["text-sm text-text dark:text-background font-medium inline-block", @required && "required"]}>
      <%= render_slot(@inner_block) %>
    </label>
    """
  end
end
