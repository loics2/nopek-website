defmodule Nopek.Content do
  alias Nopek.Content.Part

  use NimblePublisher,
    build: Part,
    from: "./pages/content/*.md",
    as: :parts

  def all_parts(), do: @parts

  def get_part_by_id(id), do: all_parts() |> Enum.find(&(&1.id == id))
end
