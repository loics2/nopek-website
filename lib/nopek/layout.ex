defmodule Nopek.Layout do
  use Phoenix.Component

  import Nopek.Components

  attr :class, :string, default: nil

  slot :inner_block, required: true

  def layout(assigns) do
    ~H"""
      <!DOCTYPE html>
      <html lang="en">
        <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.2/font/bootstrap-icons.min.css">
          <link rel="stylesheet" href="/assets/app.css" />
          <title>Le Nopek</title>
        </head>
        <body class={@class}>
          <main class="md:min-h-screen">
            <%= render_slot(@inner_block) %>
          </main>
          <.footer>
            <:logo>
              <img src="/img/logo_blanc.png" alt="logo smart event" class="w-48 h-48"/>
            </:logo>
            <:col label="Evènement labelisé">
              <img src="/img/logo_smart_event.png" alt="logo smart event"/>
            </:col>
            <:col label="Avec le soutien de">
              <img src="/img/logo_fribourg.png" alt="logo Ville de Fribourg"/>
            </:col>
            <:col label="Et de">
              <img src="/img/logo_loro.jpg" alt="logo Lotterie romande"/>
            </:col>
          </.footer>
          <script type="text/javascript" src="/assets/app.js" />
        </body>
      </html>
    """
  end
end
