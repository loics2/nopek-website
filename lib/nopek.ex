defmodule Nopek do
  use Phoenix.Component

  import Phoenix.HTML
  import Nopek.{Components, Layout}

  alias Nopek.{Content, Performers}

  @output_dir "./public"
  File.mkdir_p!(@output_dir)

  def build() do
    performers = Performers.all_performers()
    intro = Content.get_part_by_id("intro")
    info = Content.get_part_by_id("info")
    assoc = Content.get_part_by_id("assoc")

    render_file(
      "index.html",
      index(%{
        performers: performers,
        intro: intro,
        info: info,
        assoc: assoc,
        form_key: "927cb66c-15c9-43db-a345-6cac2272ca10"
      })
    )

    render_file("404.html", not_found(%{}))

    File.cp_r!("assets/static", @output_dir)

    for performer <- performers do
      dir = Path.dirname(performer.path)

      if dir != "." do
        File.mkdir_p!(Path.join([@output_dir, dir]))
      end

      render_file(performer.path, performer(%{performer: performer}))
    end

    :ok
  end

  def render_file(path, rendered) do
    safe = Phoenix.HTML.Safe.to_iodata(rendered)
    output = Path.join([@output_dir, path])
    File.write!(output, safe)
  end

  def performer(assigns) do
    ~H"""
    <.layout class="bg-background dark:bg-text md:bg-background-50 md:dark:bg-text-900 pt-14">
      <.hero title={@performer.name}>
        <:subtitle><%= @performer.description %></:subtitle>
        <:image>
          <img src={@performer.image} alt={"image de #{@performer.name}"} class="rounded-md"/>
        </:image>
      </.hero>
      <a class="flex items-center justify-center w-10 h-10 rounded-full bg-primary text-background fixed top-10 md:top-20 left-10 md:left-64 hover:w-12 hover:h-12 transition-all" href="/">
        <span class="bi bi-arrow-left"></span>
      </a>
      <section class="bg-background-50 dark:bg-text-900 py-20">
        <div class="container mx-auto p-5 prose prose-h1:text-text prose-h1:dark:text-background prose-h2:text-text prose-h2:dark:text-background prose-p:text-text prose-p:dark:text-background prose-strong:text-text prose-strong:dark:text-background prose-a:text-primary prose-a:underline prose-a:underline-offset-4">
          <%= raw @performer.body %>
        </div>
      </section>
    </.layout>
    """
  end

  def index(assigns) do
    ~H"""
    <.layout>
      <.hero title="L'incontournable open air d'hiver est bientôt là">
        <:subtitle>Rendez-vous le 24 février 2024 pour les festivités!</:subtitle>
        <:actions>
          <.button id="prog-btn">
            Voir la programmation !
            <svg class="w-5 h-5 ml-2 -mr-1" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
          </.button>
        </:actions>

        <:image>
          <img src="/img/logo_noir.png" alt="Le logo du Nopek" class="block dark:hidden"/>
          <img src="/img/logo_blanc.png" alt="Le logo du Nopek" class="hidden dark:block"/>
        </:image>
      </.hero>

      <section class="bg-background-50 dark:bg-text-900 py-20">
        <div class="container mx-auto p-5 prose prose-h1:text-text prose-h1:dark:text-background prose-h2:text-text prose-h2:dark:text-background prose-p:text-text prose-p:dark:text-background prose-strong:text-text prose-strong:dark:text-background prose-a:text-primary prose-a:underline prose-a:underline-offset-4">
          <h1><%= @intro.title %></h1>
          <%= raw @intro.body %>
        </div>
      </section>

      <section id="prog" class="bg-background dark:bg-text py-20">
        <div class="container mx-auto">
          <h1>La programmation</h1>
          <div :if={not Enum.empty?(@performers)} class="flex flex-col items-center md:grid md:grid-cols-4 gap-4">
            <.card :for={performer <- @performers} name={performer.name} desc={performer.description} image={performer.image} url={performer.path}/>
          </div>
          <.empty :if={Enum.empty?(@performers)}>
            <:title>
              <h2>Rien pour l'instant</h2>
            </:title>
            <:subtitle>
              La programmation n'a pas encore été annoncée, mais reviens bientôt pour la découvrir!
            </:subtitle>
          </.empty>
        </div>
      </section>

      <section class="bg-background-50 dark:bg-text-900 py-20">
        <div class="container mx-auto p-5 prose prose-h1:text-text prose-h1:dark:text-background prose-h2:text-text prose-h2:dark:text-background prose-p:text-text prose-p:dark:text-background prose-strong:text-text prose-strong:dark:text-background prose-a:text-primary prose-a:underline prose-a:underline-offset-4">
          <h1><%= @info.title %></h1>
          <%= raw @info.body %>
        </div>
      </section>

      <section class="bg-background dark:bg-text py-20">
        <div class="container mx-auto p-5 prose prose-h1:text-text prose-h1:dark:text-background prose-h2:text-text prose-h2:dark:text-background prose-p:text-text prose-p:dark:text-background prose-strong:text-text prose-strong:dark:text-background prose-a:text-primary prose-a:underline prose-a:underline-offset-4">
          <h1><%= @assoc.title %></h1>
          <%= raw @assoc.body %>
        </div>

        <div class="container mx-auto my-8">
          <div class="flex flex-col items-center md:grid md:grid-cols-4 gap-4">
            <.card name="Kepon" image="/img/membre_kepon.jpg">
              <p>Pour les amateur·ices de PG tiède</p>
            </.card>
            <.card name="Mouette" image="/img/membre_mouette.jpg">
              <p>À moi!?</p>
            </.card>
            <.card name="Pipistrelle" image="/img/membre_pipi.jpg">
              <p>Krkkrrkrkrr y'a pipi dans le nom</p>
            </.card>
            <.card name="Elon Muscle" image="/img/membre_elon.jpg">
              <p>Un meilleur investissement que Twitter</p>
            </.card>
          </div>
        </div>

        <form action="https://api.staticforms.xyz/submit" method="post" class="container mx-auto">
          <input type="hidden" name="accessKey" value={@form_key}>
          <input type="hidden" name="redirectTo" value="https://nopek.ch">
          <input type="text" name="honeypot" style="display: none;">

          <div class="flex flex-col md:flex-row md:space-x-4">
            <.input required type="text" name="$firstName" label="Prénom"/>
            <.input required type="text" name="$lastName" label="Nom"/>
          </div>

          <.input required type="email" name="email" label="Email"/>

          <.input required type="select" name="$tier"  label="Type de membre" options={["Kepon": "kepon", "Mouette": "mouette", "Pipistrelle": "pipistrelle", "Elon Muscle": "elon"]}/>

          <.button class="mt-4" type="submit">Envoyer!</.button>
        </form>
      </section>
    </.layout>
    """
  end

  def not_found(assigns) do
    ~H"""
    <.layout>
      oops...
    </.layout>
    """
  end
end
