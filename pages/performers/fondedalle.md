%{
    name: "Foncedalle",
    description: "Trio électro-guitaresque",
    image: "/img/foncedalle.jpg"
}
---
**Foncedalle** est un trio qui brûle, un groupe amplifié à haut volume et
nourri aux beats électroniques. Il n’est ici pas question de gastronomie
tardive due à divers excès, mais plutôt d'une insatiable faim de riffs
de guitare aussi entêtants que dansants, d'une basse lourde comme le
poids du monde sur les épaules de Sisyphes et de bidouillages
électroniques aussi froids qu'un mois de décembre en 2035.

**Foncedalle** dévoile le chaînon manquant (s'il y en avait un) entre la
scène mancunienne des 80s, le rock électronique de Soulwax & les vagues
motoriques de Maserati. Pour les amateurs de turbines de guitare, de
basses qui peuvent légèrement décoller le diaphragme, et de
synthétiseurs analogiques à faible teneur en CO2.
