%{
    name: "Glaascats",
    description: "Rock intimiste",
    image: "/img/glaascats.png"
}
---
Des mélodies mélancoliques orbitent dans une atmosphère personnelle et
intime. Autour de cette bulle, une énergie incinérée et brute danse avec
insouciance. Des inspirations variées forgent ce groupe aux fils coupés,
suspendu par l’amour de la créativité.