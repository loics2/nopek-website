%{
    name: "Jean-Michel",
    description: "Punk vite fait",
    image: "/img/jean_michel.jpg"
}
---
34 albums plus tard, **Jean-Michel** s’est affirmé comme étant la
métaphore musicale de la bière Prix Garanti : C’est pas cher, c’est pas
de bon goût, mais ça fait très bien l’affaire pour faire la fête et ça
laisse un arrière goût suffisamment mauvais pour avoir envie d’en
reprendre. 

Efficace, festif, des paroles qui restent en tête, **Jean-Michel** c’est
la preuve que le rock n’est pas mort, mais le devrait.