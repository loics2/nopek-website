%{
    name: "Nathalie Froehlich",
    description: "Rappeuse ultra vénère",
    image: "/img/nathalie_froehlich.jpg"
}
---

> I’m not a queen I’m a godess.

Il y a un feu puissant et débordant qui habite certain·es artistes et
qui leur donne la capacité d’embraser un public entier en le possédant
de la tête aux pieds. **Nathalie Froehlich**, membre de la Sacrée Déter, est
l’une d’entre elles.

Rappeuse ultra vénère, sa présence exceptionnelle
est incarnée en un regard concentré et des textes aussi puissants que
directs. Sortie tout droit des raves party, elle prépare un live
énergique mélangeant break, bass, rythmes débiles et hip-hop digne d’une
after qu’on n’aurait pas envie de quitter.

Après une année 2022
fructueuse avec plus de 50 dates, 2 EPs, et diverses collaborations, la
suite s’annonce prometteuse. Nathalie sera au rendez-vous, notamment à
Paléo, Festineuch, et St-Gall Openair. Ça va chauffer, qui a le numéro
des pompiers ?
