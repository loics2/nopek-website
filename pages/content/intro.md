%{
    title: "Le Nopek c'est quoi?"
}
---
Le Nopek est un festival local, atypique et déjanté. Cet hiver, on prend
possession de **la place de la Pisciculture à Fribourg**, pour y organiser
une super fête avec la crème des groupes locaux, des bars, des stands de
nourriture et plein d’autres trucs uniques. Un open-air gratuit avec
une ambiance que tu ne trouveras nulle part ailleurs !

À l’origine, notre ancêtre, le Kopek, avait lieu chaque année au même
moment et au même endroit pendant près de dix ans. Mais après toutes ces
années, il était temps de se réinventer. Pour cette nouvelle
mouture du festival, on garde le meilleur des éditions précédentes, mais
l’accent sera mis sur l’étrange, l’atypique et le curieux plutôt que la
thématique Balkanique.

On se retrouve donc le **24 février** pour une folle journée de concerts et
de fête !