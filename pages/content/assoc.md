%{
    title: "L'association"
}
---
**Fri-gid'hair**, c'est le nom de l'association derrière le Nopek. Un nom né
après une séance de brainstorming qui s'est mal terminé, mélange de nom
standard d'association fribourgeoise, d'un salon de coiffure sans
vraiment de raison et de la marque d'appareils de réfrigération puisque
notre festival se passe en hiver.

L'association a pour but d'organiser et développer un open air à
l'esprit décalé, en proposant des concerts et performances artistiques.

Pour rentrer dans la famille **Fri-gid'hair** et nous soutenir moralement et
financièrement, il te suffit de nous envoyer tes coordonnées et choisir
ton type de cotisation.

Être membre de l'association te permet de voter durant l'Assemblée
Générale, et principalement de te vanter de donner de l'argent à une
association avec un nom pareil.

