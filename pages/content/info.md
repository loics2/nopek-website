%{
    title: "Informations"
}
---
## Le lieu

Le Nopek reprendra possession de la place de la Pisciculture, à côté
d’Emmaüs.

## Accès

Une navette sera mise en place par les TPF et circulera toutes les 30
minutes entre l’arrêt Fribourg Tivoli et le festival, de 18h30 à 2h00.

Quelques places de parking sont disponibles pour les personnes à
mobilité réduites. Des toilettes adaptées sont aussi disponibles.
N’hésitez pas à nous contacter grâce au formulaire ci-dessous pour toute
question concernant l’accès et les installations du festival.


## Bénévolat

Tu sais tirer des bières et l’arrière du bar est ta deuxième maison ?
Alors viens nous donner un coup de main ! Et si ton truc, c’est plutôt
de construire des choses, on aura besoin de monde pour le montage et
démontage du festival.

Alors n'attends plus et fonce t'inscrire sur
[nyght](https://nopek.nyght.ch)!